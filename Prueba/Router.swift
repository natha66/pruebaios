//
//  Router.swift
//  Prueba
//
//  Created by macbook on 28/11/20.
//
import UIKit
class Router {
    private var storyBoard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    func showMovieDetail(rootViewController: UIViewController, dataResult: ResultsAPI) {
        if let viewController = storyBoard.instantiateViewController(identifier: "DetailViewController") as? DetailViewController {
            viewController.dataResult = dataResult
            rootViewController.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
