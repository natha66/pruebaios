//
//  Utils.swift
//  Prueba
//
//  Created by macbook on 27/11/20.
//
import UIKit
import SwiftyJSON
import Foundation

class Utils {
    func loadImage(url: URL) -> UIImage {
        if let data = try? Data(contentsOf: url) {
            if let image = UIImage(data: data) {
                    return image
            }
        }
        return UIImage()
    }
    func setCategories(category: String) -> String {
        switch category {
        case "28":
            return "Action"
        case "12":
            return "Adventure"
        case "16":
            return "Animation"
        case "35":
            return "Comedy"
        case "80":
            return "Crime"
        case "99":
            return "Documentary"
        case "18":
            return "Drama"
        case "10751":
            return "Family"
        case "14":
            return "Fantasy"
        case "36":
            return "History"
        case "27":
            return "Horror"
        case "10402":
            return "Music"
        case "9648":
            return "Mystery"
        case "10749":
            return "Romance"
        case "878":
            return "Science Fiction"
        case "10770":
            return "TV Movie"
        case "53":
            return "Thriller"
        case "10752":
            return "War"
        case "37":
            return "Western"
        default: return "N/A"
        }
    }
}
/*
 MOVIE
 Action          28
 Adventure       12
 Animation       16
 Comedy          35
 Crime           80
 Documentary     99
 Drama           18
 Family          10751
 Fantasy         14
 History         36
 Horror          27
 Music           10402
 Mystery         9648
 Romance         10749
 Science Fiction 878
 TV Movie        10770
 Thriller        53
 War             10752
 Western         37
 
 
 PELÍCULA
 Acción 28
 Aventura 12
 Animación 16
 Comedia 35
 Crimen 80
 Documental 99
 Drama 18
 Familia 10751
 Fantasía 14
 Historia 36
 Horror 27
 Música 10402
 Misterio 9648
 Romance 10749
 Ciencia Ficción 878
 Película de televisión 10770
 Thriller 53
 Guerra 10752
 Occidental 37
 */
public enum TextInTheApp: String {
    case title = "Películas"
    case errorService = "Error no se pudo hacer contacto con el servicio"
    case popularity = "Popularidad"
    case releaseDate = "Fecha de estreno"
    case qualification = "Calificacón"
    case genres = "Géneros"
    case description = "Descripción"
}
public enum MovieCategories: String {
    case Action
    case Adventure
    case Animation
    case Comedy
    case Crime
    case Documentary
    case Drama
    case Family
    case Fantasy
    case History
    case Horror
    case Music
    case Mystery
    case Romance
    case Science_Fiction = "Science Fiction"
    case TV_Movie = "TV Movie"
    case Thriller
    case War
    case Western
}
struct URLText {
    let publicKey: String = "?api_key=eb967e493e7b0a47aaa3e442c536ab89"
    let baseUrl: String = "https://api.themoviedb.org/3/movie/now_playing"
    let baseUrlImages: String = "https://image.tmdb.org/t/p/w500/"
}
public struct PageResults {
  public var page: Int?
  public var total_results: Int?
  public var total_pages: Int?
  public var results: [ResultsAPI]?
  public init(page: Int, total_results: Int, total_pages: Int, results: [ResultsAPI]?) {
    self.page = page
    self.total_results = total_results
    self.total_pages = total_pages
    self.results = results
  }
  init(intialize: Bool) {
    self.page = nil
    self.total_results = nil
    self.total_pages = nil
    self.results = nil
  }
}
public struct ResultsAPI {
    public var popularity: Float?
    public var adult: Bool?
    public var backdrop_path: String?
    public var poster_path: String?
    public var genre_ids: [String]?
    public var original_language: String?
    public var original_title: String?
    public var overview: String?
    public var video: Bool?
    public var vote_average: Float?
    public var id: Int?
    public var vote_count: Int?
    public var release_date: String?
    public var title: String?
    init(popularity: Float, adult: Bool, backdrop_path: String, poster_path: String, genre_ids: [String], original_language: String, original_title: String, overview: String, video: Bool, vote_average: Float, id: Int, vote_count: Int, release_date: String, title: String) {
        self.popularity = popularity
        self.adult = adult
        self.backdrop_path = backdrop_path
        self.poster_path = poster_path
        self.genre_ids = genre_ids
        self.original_language = original_language
        self.original_title = original_title
        self.overview = overview
        self.video = video
        self.vote_average = vote_average
        self.id = id
        self.vote_count = vote_count
        self.release_date = release_date
        self.title = title
    }
}
public struct ReturnAPI {
    public var error: Error?
    public var data: Data?
    public var response: URLResponse?
    init(error: Error?, data: Data?, reponse: URLResponse?) {
        self.error = error
        self.data = data
        self.response = reponse
    }
}
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
