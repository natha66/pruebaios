//
//  DetailViewController.swift
//  Prueba
//
//  Created by macbook on 28/11/20.
//

import UIKit
class DetailViewController: UIViewController {
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet var titleMovieLabel: UILabel!
    @IBOutlet var labels: [UILabel]!
    let utils: Utils = Utils()
    var dataResult: ResultsAPI?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyles()
    }
    func setStyles() {
        guard let data = dataResult else { return }
        if let urlBackdrop = URL(string: data.backdrop_path ?? "") {
            self.movieImage.image = utils.loadImage(url: urlBackdrop)
        }
        self.titleMovieLabel.text = "\(data.title ?? "")"
        for label in self.labels {
            switch label.tag {
            case 1:
                label.text = TextInTheApp.popularity.rawValue
            case 2:
                label.text = "\(data.popularity ?? 0.0)"
            case 3:
                label.text = TextInTheApp.releaseDate.rawValue
            case 4:
                label.text = data.release_date
            case 5:
                label.text = TextInTheApp.qualification.rawValue
            case 6:
                label.text = "\(data.vote_average ?? 0.0)"
            case 7:
                label.text = TextInTheApp.genres.rawValue
            case 8:
                var genreString: String = ""
                for genre in data.genre_ids ?? [] {
                    genreString = genreString.isEmpty ? utils.setCategories(category: genre) : genreString + ", " + utils.setCategories(category: genre)
                }
                label.text = genreString
            case 9:
                label.text = TextInTheApp.description.rawValue
            case 10:
                label.text = data.overview
            default: break
            }
        }
    }
}
