//
//  BaseViewController.swift
//  Prueba
//
//  Created by macbook on 27/11/20.
//
import UIKit
class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewBaseSetStyles()
    }
    func viewBaseSetStyles() {
        navigationController?.navigationBar.barTintColor = UIColor.white
    }
}
