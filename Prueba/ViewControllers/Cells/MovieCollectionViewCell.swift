//
//  MovieCollectionViewCell.swift
//  Prueba
//
//  Created by macbook on 27/11/20.
//
import UIKit
class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ratingsLabel: UILabel!
    @IBOutlet weak var logoImage: UIImageView!
}
