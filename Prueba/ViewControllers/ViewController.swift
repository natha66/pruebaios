//
//  ViewController.swift
//  Prueba
//
//  Created by macbook on 27/11/20.
//

import UIKit

class ViewController: BaseViewController {
    @IBOutlet var collectionView: UICollectionView!
    let presenter: Presenter = Presenter()
    let utils: Utils = Utils()
    fileprivate var mydata: PageResults? {
            willSet {
                self.collectionView.reloadData()
            }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyles()
    }
    func setStyles() {
        self.title = TextInTheApp.title.rawValue
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        presenter.getDataPresenter { (data, error) in
            self.mydata = data
        }
    }
}
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mydata?.results?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCollectionViewCell", for: indexPath) as? MovieCollectionViewCell {
            guard let data = mydata?.results else { return UICollectionViewCell() }
            if let urlPoster = URL(string: data[indexPath.item].poster_path ?? "") {
                cell.movieImage.image = utils.loadImage(url: urlPoster)
            }
            cell.dateLabel.text = data[indexPath.item].release_date
            cell.titleLabel.text = data[indexPath.item].title
            cell.ratingsLabel.text = "\(data[indexPath.item].vote_average ?? 0.0)"
            cell.logoImage.image = UIImage(named: "Star")
            cell.layer.cornerRadius = 10
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let data = mydata?.results else { return }
        self.presenter.showMovieDetailPresenter(rootViewController: self, resultData: data[indexPath.row])
    }
}
