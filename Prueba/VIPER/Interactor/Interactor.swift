//
//  Interactor.swift
//  Prueba
//
//  Created by macbook on 28/11/20.
//
import Alamofire
import Foundation
import SwiftyJSON

class Interactor {
    func getData(returnData: @escaping (_ data: PageResults? , _ error: String?) -> Void) {
        var entityData: PageResults? = PageResults.init(intialize: true)
        let urlApi =  URLText.init().baseUrl + URLText.init().publicKey
        let AlamoManager = Alamofire.Session.default
        AlamoManager.request(urlApi, method: .get)
            .responseJSON { (response) in
                print(response)
                switch response.result {
                    case .success:
                        if let data = response.data {
                            let responseJSON = try! JSON(data: data)
                                var resultArray: [ResultsAPI]? = []
                                for result in responseJSON["results"] {
                                    var genreArray: [String] = []
                                    for genre in result.1["genre_ids"].arrayValue {
                                        genreArray.append("\(genre)")
                                    }
                                    resultArray?.append(ResultsAPI(popularity: result.1["popularity"].floatValue, adult: result.1["adult"].boolValue, backdrop_path: URLText.init().baseUrlImages + result.1["backdrop_path"].stringValue, poster_path: URLText.init().baseUrlImages + result.1["poster_path"].stringValue, genre_ids: genreArray, original_language: result.1["original_language"].stringValue, original_title: result.1["original_title"].stringValue, overview: result.1["overview"].stringValue, video: result.1["video"].boolValue, vote_average: result.1["vote_average"].floatValue, id: result.1["id"].intValue, vote_count: result.1["vote_count"].intValue, release_date: result.1["release_date"].stringValue, title: result.1["title"].stringValue))
                                }
                                entityData = PageResults.init(page: responseJSON["page"].intValue, total_results: responseJSON["total_results"].intValue, total_pages: responseJSON["total_pages"].intValue, results: resultArray)
                                returnData(entityData, nil)
                        } else {
                            returnData(nil, TextInTheApp.errorService.rawValue)
                        }
                    case .failure:
                        print("Error")
                        returnData(nil, TextInTheApp.errorService.rawValue)
                }
        }
    }
}
class HTTPRequest{
  class func request(_ url: String, httpMethod: String, parameters: [String: AnyObject]?, completionHandler: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> ()) -> (){
    let urlString = url
    let requestURL = URL(string: urlString)!
    let request = NSMutableURLRequest(url: requestURL)
    request.httpMethod = httpMethod
    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    
    let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
      DispatchQueue.main.async(execute: { () -> Void in
        if error != nil{
          print("Error -> \(String(describing: error))")
          completionHandler(nil, nil, error as Error?)
        }else{
          completionHandler(data, response, nil)
        }
      })
      
    })
    task.resume()
  }
}
