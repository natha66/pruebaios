//
//  Presenter.swift
//  Prueba
//
//  Created by macbook on 28/11/20.
//
import UIKit
class Presenter {
    let interactor: Interactor = Interactor()
    let router: Router = Router()
    func getDataPresenter(returnData: @escaping (_ data: PageResults? , _ error: String?) -> Void) {
        interactor.getData { (data, error) in
            returnData (data, error)
        }
    }
    func showMovieDetailPresenter(rootViewController: UIViewController, resultData: ResultsAPI) {
        router.showMovieDetail(rootViewController: rootViewController, dataResult: resultData)
    }
}
